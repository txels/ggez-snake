# Snake game built on ggez

[ggez](https://ggez.rs) is a game engine written in Rust.

This repo contains an extended version of the Snake example from the ggez repo that I tweaked in order to learn how to use the engine.

Changes introduced:

 - Keeping score
 - The game accelerates as you eat and the snake grows
 - You have a limited time to eat, else you die.
